<?php


function psr4Autoload($className) {
    include implode(DIRECTORY_SEPARATOR, array_slice(explode('\\', $className), 1)) . '.php';
}


$rootPath = realpath(implode(DIRECTORY_SEPARATOR, [__DIR__, '..']));

$settingsPathOfTests =  implode(DIRECTORY_SEPARATOR, [$rootPath, 'src', 'settings']);

$pathIncluder = implode(DIRECTORY_SEPARATOR, [$settingsPathOfTests, 'path4autoload-includer.php']);


require_once $pathIncluder;


spl_autoload_register('psr4Autoload');
