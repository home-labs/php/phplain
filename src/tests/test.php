<?php


require 'psr-4-autoload-index.php';


use PHPlain\PHPlainObject;


class ClassA extends PHPlainObject {

    function setAttribute1($value) {
        parent::setAttribute1($value);
    }

    function getAttribute1() {
        return parent::getAttribute1();
    }

}


$class = new ClassA();
$class->setAttribute1('Foo');

print_r($class->getAttribute1());

// version (tag on git): "v1.0.0-RC1", mas não usar no gerenciador de pacote
