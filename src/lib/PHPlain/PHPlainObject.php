<?php


namespace PHPlain;


class PHPlainObject {

    private $attributeNames;

    function __construct() {
        $this->attributeNames = [];
    }

    function __call($attributeName, $value) {
        $getPattern = "/^get/";

        $setPattern = "/^set/";

        if (preg_match($getPattern, $attributeName)) {

            $suffix = preg_replace($getPattern, '', $attributeName);

            if (array_key_exists($suffix, $this->attributeNames)) {
                return $this->attributeNames[$suffix];
            }

            return null;
        } else if (preg_match($setPattern, $attributeName)) {
            $suffix = preg_replace($setPattern, '', $attributeName);
            $this->attributeNames[$suffix] = $value[0];
        }

    }

}
